using HaykathDevLab.AbilitySystem.Runtime;
using HaykathDevLab.AbilitySystem.Tests.Builders;
using UnityEngine;
using UnityEngine.TestTools;

namespace HaykathDevLab.AbilitySystem.Tests.Components
{
    public class ChanneledAbilityTest : MonoBehaviour, IMonoBehaviourTest, IAbilityUser
    {
        public const string CastMessage = "Channel tick";
        public static float InterruptTime = 1f;

        public GameObject GameObject => gameObject;

        public Vector3 CastPoint => transform.position;
        public Vector3 TargetPoint { get; } = Vector3.zero;
        public GameObject TargetObject { get; } = null;

        public bool IsTestFinished { get; private set; }

        private Ability ability;
        private float startTime;

        private void Start() {
            IsTestFinished = false;
            ability = AbilityBuilder.Create
                .Channeled()
                .WithChannelTickTime(.3f)
                .WithCastAction(ActionBuilder.Message(CastMessage))
                .DontApplyFirstChannelTick();
                
            LogAssert.Expect(LogType.Log, CastMessage);
            LogAssert.Expect(LogType.Log, CastMessage);
            LogAssert.Expect(LogType.Log, CastMessage);

            startTime = Time.time;
            StartCoroutine(ability.Execute(this));
        }

        private void Update() {
            if (Time.time >= startTime + InterruptTime)
            {
                ability.Interrupt();
                IsTestFinished = true;
            }
        }
    }
}