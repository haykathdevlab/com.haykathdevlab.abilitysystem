using HaykathDevLab.AbilitySystem.Runtime;
using HaykathDevLab.AbilitySystem.Runtime.Actions;

namespace HaykathDevLab.AbilitySystem.Tests.Builders
{
    public class ActionBuilder
    {
        public static IAbilityAction Message(string message) {
            return new LogAction
            {
                Message = message
            };
        }
    }
}