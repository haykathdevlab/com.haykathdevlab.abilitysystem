using HaykathDevLab.AbilitySystem.Runtime;

namespace HaykathDevLab.AbilitySystem.Tests.Builders
{
    public static class CostBuilder
    {
        public static IAbilityCost ResourceCost(ResourceHandler handler, int cost) {
            return new ResourceCost(handler, cost);
        }
    }
}