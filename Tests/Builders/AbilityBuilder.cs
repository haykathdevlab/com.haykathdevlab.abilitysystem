using System.Collections.Generic;
using HaykathDevLab.AbilitySystem.Runtime;

namespace HaykathDevLab.AbilitySystem.Tests.Builders
{
    public class AbilityBuilder
    {
        public static AbilityBuilder Create => new AbilityBuilder();

        public static implicit operator Ability(AbilityBuilder builder) => builder.ToAbility();
        
        private AbilityData abilityData;

        private AbilityBuilder() {
            abilityData = new AbilityData();
        }

        public AbilityBuilder WithCastAction(IAbilityAction action) {
            if (action == null) return this;
            
            if (abilityData.OnCast == null)
            {
                abilityData.OnCast = new List<IAbilityAction>();
            }
            
            abilityData.OnCast.Add(action);

            return this;
        }
        
        public AbilityBuilder WithBeforeCastAction(IAbilityAction action) {
            if (action == null) return this;
            
            if (abilityData.OnBeforeCast == null)
            {
                abilityData.OnBeforeCast = new List<IAbilityAction>();
            }
            
            abilityData.OnBeforeCast.Add(action);

            return this;
        }
        
        public AbilityBuilder WithAfterCastAction(IAbilityAction action) {
            if (action == null) return this;
            
            if (abilityData.OnAfterCast == null)
            {
                abilityData.OnAfterCast = new List<IAbilityAction>();
            }
            
            abilityData.OnAfterCast.Add(action);

            return this;
        }
        
        public AbilityBuilder WithProjectileHitAction(IAbilityAction action) {
            if (action == null) return this;
            
            if (abilityData.OnProjectileHit == null)
            {
                abilityData.OnProjectileHit = new List<IAbilityAction>();
            }
            
            abilityData.OnProjectileHit.Add(action);

            return this;
        }

        public AbilityBuilder WithCooldown(float cooldown)
        {
            abilityData.Cooldown = cooldown;
            return this;
        }

        public AbilityBuilder WithCost(IAbilityCost cost)
        {
            if (cost == null) return this;

            if(abilityData.Costs == null)
            {
                abilityData.Costs = new List<IAbilityCost>();
            }

            abilityData.Costs.Add(cost);

            return this;
        }

        public AbilityBuilder Channeled(bool isChanneled = true)
        {
            abilityData.IsChanneled = isChanneled;
            return this;
        }

        public AbilityBuilder WithChannelTickTime(float tickTime)
        {
            abilityData.ChannelTickTime = tickTime;
            return this;
        }

        public AbilityBuilder WithMaxChannelTime(float channelTime) {
            abilityData.MaxChannelTime = channelTime;
            return this;
        }
        
        public AbilityBuilder ApplyCostsOnChannelTick(bool applyCosts = true)
        {
            abilityData.ApplyCostsOnChannelTick = applyCosts;
            return this;
        }
        
        public AbilityBuilder DontApplyFirstChannelTick(bool apply = true)
        {
            abilityData.DontApplyFirstChannelTick = apply;
            return this;
        }        

        private Ability ToAbility() {
            return new Ability(abilityData);
        }
    }
}