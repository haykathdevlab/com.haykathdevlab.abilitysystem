﻿using System.Collections;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    public interface IAbilityAction
    {
        IEnumerator Execute(Ability owner, Vector3 castPoint, Vector3 targetPoint, GameObject targetObj);
    }
}
