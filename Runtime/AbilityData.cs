using System.Collections.Generic;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    public struct AbilityData
    {
        public float Cooldown;
        public bool IsChanneled;
        public float ChannelTickTime;
        public float MaxChannelTime;
        public bool ApplyCostsOnChannelTick;
        public bool DontApplyFirstChannelTick;

        public List<IAbilityCost> Costs;
        
        public List<IAbilityAction> OnBeforeCast;
        public List<IAbilityAction> OnCast;
        public List<IAbilityAction> OnAfterCast;
        public List<IAbilityAction> OnProjectileHit;
    }
}