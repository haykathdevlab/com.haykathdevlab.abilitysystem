﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    [Serializable]
    public class Blackboard
    {
        [SerializeField] private Dictionary<string, object> blackboard;

        public T Get<T>(string key) {
            if (!blackboard.TryGetValue(key, out var data))
            {
                throw new Exception($"Blackboard does not contain the key {key}");
            }

            if (!(data is T result))
            {
                throw new Exception($"The valued stored for key {key} in the blackboard is not of type {typeof(T)}");
            }

            return result;
        }

        public void Set<T>(string key, T value) {
            if(blackboard.ContainsKey(key) && !(blackboard[key] is T))
                Debug.LogWarning($"Replacing variable type for key {key} in blackboard.");

            blackboard[key] = value;
        }

        public bool TryGet<T>(string key, out T value) {
            if (!blackboard.TryGetValue(key, out var data) || !(data is T result))
            {
                value = default;
                return false;
            }

            value = result;
            return true;
        }
    }
}
