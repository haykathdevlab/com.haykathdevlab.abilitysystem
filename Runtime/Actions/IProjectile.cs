using System.Collections;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Actions
{
    public delegate IEnumerator OnProjectileHitEventHandler(Collision collisionData);
    
    public interface IProjectile
    {
        event OnProjectileHitEventHandler OnProjectileHit;
        Vector3 Velocity { get; set; }
    }
}