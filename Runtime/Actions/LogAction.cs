using System.Collections;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Actions
{
    public class LogAction : IAbilityAction
    {
        public string Message = "";

        public IEnumerator Execute(Ability owner, Vector3 castPoint, Vector3 targetPoint, GameObject targetObj) {
            Debug.Log(Message);
            yield return null;
        }
    }
}