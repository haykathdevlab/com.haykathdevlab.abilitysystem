using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime
{
    public interface IAbilityUser
    {
        GameObject GameObject { get; }
        
        Vector3 CastPoint { get; }
        
        Vector3 TargetPoint { get; }

        GameObject TargetObject { get; }
    }
}