using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Utils
{
    public class ProjectilePoolManager : MonoBehaviour
    {
        private static ProjectilePoolManager _instance;

        public static ObjectPool Pool => Instance.pool;

        public static ProjectilePoolManager Instance {
            get {
                if (_instance) return _instance;
                
                var obj = new GameObject(nameof(ProjectilePoolManager));
                return obj.AddComponent<ProjectilePoolManager>();
            }
        }

        private ObjectPool pool;

        private void Awake() {
            if (_instance)
            {
                Destroy(this);
                return;
            }
            _instance = this;
            
            pool = new ObjectPool(this);
        }

        
    }
}