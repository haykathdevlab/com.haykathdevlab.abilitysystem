﻿using System.Collections;
using HaykathDevLab.AbilitySystem.Runtime.Actions;
using HaykathDevLab.AbilitySystem.Runtime.Utils;
using UnityEngine;

namespace HaykathDevLab.AbilitySystem.Runtime.Projectiles
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyProjectile : MonoBehaviour, IProjectile
    {
        public event OnProjectileHitEventHandler OnProjectileHit;

        public Vector3 Velocity {
            get => _rigidbody != null ? _rigidbody.velocity : Vector3.zero;
            set {
                if (_rigidbody)
                    _rigidbody.velocity = value;
            }
        }

        [SerializeField] private bool destroyOnHit = true;
        private Rigidbody _rigidbody;

        private void Awake() {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other) {
            if (OnProjectileHit != null)
            {
                StartCoroutine(OnHit(other));
            }
        }
        
        private IEnumerator OnHit(Collision collisionData) {
            if (OnProjectileHit != null)
            {
                yield return OnProjectileHit(collisionData);
            }
            if (destroyOnHit)
            {
                ProjectilePoolManager.Pool.Dispose(gameObject);
                //Destroy(gameObject);
            }
        }
    }
}